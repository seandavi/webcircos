<?php
date_default_timezone_set('America/New_York');
error_reporting(0);
$username = $_POST['userID'];
// no need giving a notification that the ID doesn't exist if they haven't entered one yet. I did a calculation and found that there's actually a
// 7*10E-34% chance that the user ID will be blank. Funny if that were to happen.
if($username == '') {
	exit;
}
if(is_dir("temp/" . $username) == false) {
	echo "<script> window.alert(\"ID does not match any sessions\");
	if(get_cookie('rcircostoken') != '') {
		forgetCookie();
	}
	$('#start').css('display', 'none');
	$('#deleteFiles').css('display', 'none');";
	exit;
}
$outDir="temp/" . $username ."/";
// since the file names are used in the program characters that could possibly run malicious code need to be removed
	$illegal = array(':', '|','/', '\\', '(', ')', '~', "'", '"', "?", "#", "$", "%", "&", ",", "!", "@", "^", "*", ";", "[", "]", "{", "}", "<",">","+", "-", " ", "=");
if($_FILES["file1"]["tmp_name"]=='') {
	$file1 = '';
} else {
	$name = $_FILES["file1"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file1 = $outDir . $name;
	move_uploaded_file($_FILES["file1"]["tmp_name"], $file1);
}
if($_FILES["file2"]["tmp_name"]=='') {
	$file2 ='';
} else {
	$name = $_FILES["file2"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file2 = $outDir . $name;
	move_uploaded_file($_FILES["file2"]["tmp_name"], $file2);
}
if($_FILES["file3"]["tmp_name"]=='') {
	$file3 ='';
} else {
	$name = $_FILES["file3"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file3 = $outDir . $name;
	move_uploaded_file($_FILES["file3"]["tmp_name"], $file3);
}
if($_FILES["file4"]["tmp_name"]=='') {
	$file4='';
} else {
	$name = $_FILES["file4"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file4 = $outDir . $name;
	move_uploaded_file($_FILES["file4"]["tmp_name"], $file4);
}
if($_FILES["file5"]["tmp_name"]=='') {
	$file5='';
} else {
	$name = $_FILES["file5"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file5 = $outDir . $name;
	move_uploaded_file($_FILES["file5"]["tmp_name"], $file5);
}
if($_FILES["file6"]["tmp_name"]==''){
	$file6 = '';
} else {
	$name = $_FILES["file6"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file6 =$outDir . $name;
	move_uploaded_file($_FILES["file6"]["tmp_name"], $file6);
}
if($_FILES["file7"]["tmp_name"] =='') {
	$file7 = '';
} else {
	$name = $_FILES["file7"]["name"];
	foreach($illegal as $illchar) {
		$name = str_replace($illchar, 'R', $name);
	}
	$file7 = $outDir . $name;
	move_uploaded_file($_FILES["file7"]["tmp_name"], $file7);
}
$images =  glob($outDir . 'hist/*'); 
// sort the images so that new plots show up at the bottom
usort($images, function($a, $b) {
	return filemtime($b) < filemtime($a);
});
$useFiles = $_POST['delList'];
$c = 0;
$geneFiles = array();
$linkFiles = array();
$heatmapFiles = array();
$histogramFiles = array();
$lineScatFiles = array();
$tileFiles = array();
$n1 = 0;
$files = glob($outDir . '*');
// sort the files so that new files show up at the bottom
usort($files, function($a, $b) {
	return filemtime($b) < filemtime($a);
});
// keep track of the number of files and images so that the canvas can be generated only as big as it's needed
foreach ($files as $allfile1) {
	if(substr($allfile1, strlen($username) + 6) != 'hist' & substr($allfile1, strlen($username) + 6) != 'relations.txt' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzzz' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzz') {
		$n1 += 1;
	}
}
$n2 = 0;
foreach (glob($outDir . '/hist/*') as $allfile1) {
	$n2 += 1;
}
if($n1 > $n2) {
	$canvasNum = $n1;
} else {
	$canvasNum = $n2;
}
$rowNum = $canvasNum +1;
$canvasNum *= 80;
// initializing the table. all of this will be the same regardless of how many files there are (except canvas size)
echo "<table id='fileManager' width=\"400\" align=\"center\" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
		<tr><td><input type='checkbox' id='toggle' checked onchange='Javascript:toggleCheckboxes()'></input></td><td>Toggle Selection</td></tr>
		<tr><td>&nbsp;</td><td>Filename</td><td>Date uploaded</td><td>Filesize</td><td rowspan=$rowNum><canvas width='300' height='$canvasNum' id='relationsCanvas'></canvas></td></tr>";
// for every file create a new row and fill it with the information about it
foreach ($files as $allfile1) {
	if(substr($allfile1, strlen($username) + 6) != 'hist' & substr($allfile1, strlen($username) + 6) != 'relations.txt' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzzz' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzz' & substr($allfile1, strlen($username) + 6) != 'logs') {
		$time = filemtime($allfile1);
		$class = substr($allfile1, 6 + strlen($username));
		// gah, more lazy programming where I get rid of the file extension because it messes with the class
		$classExplode = explode(".", $class);
		$class = $classExplode[0];
		echo "<tr style='display: table-row'><td height='70' class='". $class ."'><input id='$allfile1' type='checkbox' class='". $class ."' value=" .  $allfile1. "
				name='delList[]' checked></input></td>";
		echo "<td class='". $class ."'>" . substr($allfile1, strlen($username) + 6) . "</td>";
		$filetime = filemtime($allfile1);
		$date = date("F d Y H:i:s.", $filetime);
		echo "<td class='". $class ."'>" . $date . "</td>";
		echo "<td id='".$class."' class='". $class ."'>" . round(filesize($allfile1)/1024, 1) . " KB</td>";
		if (empty($images[$c]) == false) {
			$imgClass = substr($images[$c], 11 + strlen($username));
			$imgExplode = explode(".", $imgClass);
			$imgClass = $imgExplode[0];
			if(substr($images[$c], -3) == 'pdf') {
				echo "<td></td><td><a id='$images[$c]' class='$imgClass' onclick=\"Javascript: $('#historySelect').val('$images[$c]'); histUpdate();\" target='_blank'>Download PDF</a></td><td height='70' class='". $imgClass ."'><input id='$c' type='checkbox' value=$images[$c] name='delimgList[]'checked onchange='Javascript:toggleRelationCheckboxes($c)'></input></td>";
			} else {
				echo "<td></td><td><a id='$images[$c]' class='$imgClass' onclick=\"Javascript: $('#historySelect').val('$images[$c]'); histUpdate();\" target='_blank'><img align='center' src='". $images[$c] ."' height='40' width='40'></a></td>
						<td height='70' class='". $imgClass ."'><input id='$c' type='checkbox' value=$images[$c] name='delimgList[]' checked onchange='Javascript:toggleRelationCheckboxes($c)'></input></td>";
			}
		}
		echo "</tr>";
		$q=FALSE;
		$fh = fopen($allfile1, 'r');
		if('txt' == substr($allfile1, -3)) {
			$data = fgetcsv($fh, "\t");
			$data2 = explode("\t", $data[0]);
			if($data2[3] == 'Gene') {
				$geneFiles[$c] = $allfile1;
			} elseif ($data2[3] == 'GeneName') {
				$heatmapFiles[$c] = $allfile1;
			} elseif ($data2[3] == 'Data') {
				$histogramFiles[$c] = $allfile1;
			} elseif ($data2[3] == 'num.mark') {
				$lineScatFiles[$c] = $allfile1;
			} elseif (empty($data2[3]) && $data2[0] == 'Chromosome') {
				$tileFiles[$c] = $allfile1;
			} elseif (substr($data2[3], 0, 11) == 'Chromosome.') {
				$linkFiles[$c] = $allfile1;
			}
		} else {
			$data = fgetcsv($fh, ",");
			if($data[3] == 'Gene') {
				$geneFiles[$c] = $allfile1;
			} elseif ($data[3] == 'GeneName') {
				$heatmapFiles[$c] = $allfile1;
			} elseif ($data[3] == 'Data') {
				$histogramFiles[$c] = $allfile1;
			} elseif ($data[3] == 'num.mark') {
				$lineScatFiles[$c] = $allfile1;
			} elseif (empty($data[3]) && $data[0] == 'Chromosome') {
				$tileFiles[$c] = $allfile1;
			} elseif (substr($data[3], 0, 11) == 'Chromosome.') {
				$linkFiles[$c] = $allfile1;
			}
		}
		$c++;
	} 
}
foreach($files as $allfile1) {
	if (substr($allfile1, strlen($username)+6, -6) == 'zzzzzzz' | substr($allfile1, strlen($username)+6, -6) == 'zzzzzz' | $allfile1 == 'hist') {
		if (empty($images[$c]) == false) {
			$imgClass = substr($images[$c], 11 + strlen($username));
			$imgExplode = explode(".", $imgClass);
			$imgClass = $imgExplode[0];
			if(substr($images[$c], -3) == 'pdf') {
				echo "<tr height='70'><td></td><td></td><td></td><td></td><td></td><td><a id='$images[$c]' class='$imgClass' onclick=\"Javascript: $('#historySelect').val('$images[$c]'); histUpdate();\" target='_blank'>Download PDF</a></td><td class='". $imgClass ."'><input id='$c' type='checkbox' value=$images[$c] name='delimgList[]' checked onchange='Javascript:toggleRelationCheckboxes($c)'></input></td></tr>";
			} else {
				echo "<tr height='70'><td></td><td></td><td></td><td></td><td></td><td>" . "<a id='$images[$c]' class='$imgClass' onclick=\"Javascript: $('#historySelect').val('$images[$c]'); histUpdate();\" target='_blank'><img align='center' src='". $images[$c] ."' height='40' width='40'></a></td><td class='". $imgClass ."'><input id='$c' type='checkbox' value=$images[$c] name='delimgList[]' checked onchange='Javascript:toggleRelationCheckboxes($c)'></input></td></tr>";
			}
		}
			$c++;
	}
}
// create an option in the history bar for each file in the history folder
$histScript = '';
foreach (glob($outDir . 'hist/*') as $allfile1) {
	$filename = substr($allfile1, strlen($username) + 11);
	$histScript .= "<option value='$allfile1'>$filename</option>";
}
echo "</table>";
// this parses each file and looks at the first line. if the first line matches a recognized plot type then it adds it to the drop down menu 
// for that type
if(empty($heatmapFiles)) {
	$heatmapScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$heatmapScript .= "<option value=''>No file</option>";
	foreach ($heatmapFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$heatmapScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
if(empty($histogramFiles)) {
	$histogramScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$histogramScript .= "<option value=''>No file</option>";
	foreach ($histogramFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$histogramScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
if(empty($lineScatFiles)) {
	$lineScatScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$lineScatScript .="<option value=''>No file</option>";
	foreach ($lineScatFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$lineScatScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
if(empty($tileFiles)) {
	$tileScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$tileScript .= "<option value=''>No file</option>";
	foreach ($tileFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$tileScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
if(empty($geneFiles)) {
	$geneScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$geneScript .= "<option value=''>No file</option>";
	foreach ($geneFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$geneScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
if(empty($linkFiles)) {
	$linkScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$linkScript .= "<option value=''>No file</option>";
	foreach ($linkFiles as $file) {
		if(in_array($file, $useFiles)) {
			$filename = substr($file, strlen($username) + 6);
			$linkScript .= "<option value='$file' selected='selected'>$filename</option>";
		}
	}
}
// this draws the lines between the plots and the files and displays the rest of the filemanager
$script ="<script>var colorArray = new Array('rgba(20,83,154, .6)', 'rgba(154,110,9, .6)', 'rgba(29,110,17, .6)', 'rgba(100,20,162, .6)', 'rgba(0,0,0, .6)', 'rgba(100,0,0, .6)', 'rgba(168,168,8, .6)');
		var c = -1;
		$('#step2').css('display','table-row');
		$('#step3').css('display','table-row');
		var checkboxArray = new Array();";
if (is_file('temp/' . $username . '/relations.txt') == true) {
	$relations = file_get_contents('temp/' . $username . '/relations.txt');
	$relationsArray=explode("~", $relations);
	array_pop($relationsArray);
	foreach ($relationsArray as $relation) {
		$script .= $relation;
		$script .= "
				c++;
				checkboxArray[c] = relArray;
				if(c == 7) {
					c = 0;
				}
				var figPos = $('.' + jq(relArray[0])).offset();
				var len = relArray.length;
				var title = relArray[0] + ': ';
				for (var i= 1; i < len; i++) {
					if(i !== 1){
						title = title + ', ';
					}
					title = title + relArray[i];
					var filei = $('#' + jq(relArray[i]));
					var pos1 = filei.offset();
					DrawLine(pos1.left + filei.width(), pos1.top + filei.height()/2, figPos.left, figPos.top, colorArray[c]);
				}
				$('.' + jq(relArray[0])).prop('title', title);
				";
	}
}
$script .="	var scripts = new Array(\"$heatmapScript\", \"$histogramScript\", \"$lineScatScript\", \"$tileScript\");
		var username = document.getElementById('userID').value;
		dis = document.getElementById('user_div');
		dis.innerHTML = 'Your session ID: <a title=\"Your session ID allows you to access the files you have uploaded and the plots you have made from any computer. Just save the ID and enter it next time you visit the site\">' + username + '</a>';
		$('#username').val(username);

	$(document).ready(function() {
		$(\"[name='ph1']\").html(\"<select name='track1' id='track1'>" .$heatmapScript . "\");
		$(\"[name='ph2']\").html(\"<select name='track2' id='track2'>" .$histogramScript . "\");
		$(\"[name='ph3']\").html(\"<select name='track3' id='track3'>" .$lineScatScript . "\");
		$(\"[name='ph4']\").html(\"<select name='track4' id='track4'>" .$lineScatScript . "\");
		$(\"[name='ph5']\").html(\"<select name='track5' id='track5'>" .$tileScript . "\");
		$(\"[name='phGene']\").html(\"<select name='geneLabel' id='geneLabel'>" .$geneScript . "\");
		$(\"[name='phLink']\").html(\"<select name='linkLines' id='linkLines'>" .$linkScript . "\"); ";
	if ($histScript != '') {
		$script .= "
		$(\"#history\").html(\"History:(month:day:hour:minute:second)<select name='historySelect' id='historySelect' onchange='Javascript: histUpdate();'><option value='none'>No option selected</option>" .$histScript . "<!-- hue -->\"); ";
	}
$script .= "		});
	function connectPlotType(num) {
	var trackType = 'trackType' + num;
	var trackPh = \"track\" + num;
	var doc = document.getElementById(trackType);
	var type = doc.options[doc.selectedIndex].value;
	if(type === 'Heatmap') {
		var i = 0;
	} else if(type === 'Histogram') {
		var i = 1;
	} else if(type === 'Line' | type === 'Scatter') {
		var i = 2;
	} else if(type === 'Tile') {
		var i = 3;
	}
	var newMenu = scripts[i];
	document.getElementById(trackPh).innerHTML = newMenu;
	}
	function toggleRelationCheckboxes(num) {
		var flag = document.getElementById('' + num).checked;
		var arr = checkboxArray[num];
		var form = document.getElementById('idForm');
    	var inputs = form.elements;
    	if(!inputs){
    	    return;
    	}
    	if(!inputs.length){
    	    inputs = new Array(inputs);        
    	}
    	for (var i = 0; i < inputs.length; i++) {
    		if (inputs[i].type == 'checkbox' && arr.indexOf(inputs[i].className) > -1 ) {  
				inputs[i].checked = flag;
      		}
    	}
	}</script>";
echo $script;
?>